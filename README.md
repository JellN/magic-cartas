# Projeto Magic Store

**Magic Store Project**

## Descrição

- **Sistema de login com firebase:**
	* O usuário pode criar uma conta com email e senha (6 digitos)
	* O usuário pode recuperar a senha da conta enviando uma solicitação ao seu email
	* O usuário pode fazer o login no sistema
## 
- **Home da aplicação:**
	- Todas as informações dos produtos estão sendo retornadas do firestore
	- O usuário poderá selecionar a categoria a ser exibida na lista
	- Uma listView será criada com os itens respectivos a categoria selecionada
	- Ao clicar em um Item do ListView o usuário será redirecionado para tela com detalhes do Item (nome, imagem, preço...)
	- Após selecionar uma opção (tamanho da roupa ou versão de um jogo) o botão de adicionar ao carrinho será liberado
## 
- **Carrinho de compras:**
	- O carrinho de compras exibirá as seguintes informações:
		- Nome do produto
		- Imagem
		- Opção selecionada
		-Resumo do pedido:
			- Subtotal
			- Desconto
			- Entrega
			- Carteira
			- Empréstimo
			- Valor total
		- Botão para finalização do pedido
	- Cupons de descontos disponíveis:
		- 10OFF (10%)
		- 5OFF  (5%)
		- 15OFF (15%)
	- O usuário poderá remover um produto ao clicar na opção "remover"
	- O usuário poderá aumentar ou diminuir a quantidade do produto selecionado.
	- as informações estarão sendo atualizadas em tempo real no firestore.
	- Após finalizar o pedido uma OrderScreen será exibida com o código da sua compra, e será criado no firestore um document da sua compra e o 		produto será removido do carrinho.
	- O carrinho é uma parte importante da aplicação e suas informações permanecem salvas ao entrar e 	sair da aplicação pois uma categoria "cart" é 		gerada quando um item(document) é adicionado ao 	carrinho, o usuário poderá adicionar múltiplos produtos.
## 
- **Compras:**
	- O Usuário poderá visualizar as compras feitas na aplicação(na sua conta)
	- As informações disponíveis são:
		- Descrição do produto ( Nome, valor da peça, e valor total)
		- Status do pedido:
	- Versão que simula uma entrega de delivery onde 3 status estão disponíveis e reativos no banco de dados (preparação, transporte e entrega) ao 		mudar o valor no banco de dados essas informações se atualizam.
## 
- **Carteira:**
	- O usuário poderá visualizar o seu saldo (dinheiro).
	- O usuário poderá visualizar todo o empréstimo feito até agora
	- Os valores disponíveis inicialmente são:
		- Dinheiro: R$200,00
		- Empréstimo: R$00,00
	- Esses números serão atualizados a partir de cada compra, quando o dinheiro total do usuário 	chegar a 0, o empréstimo será ativado e somado a 	cada compra, onde será exibido em tempo real na aplicação, na opção de carteira. Ao fazer a compra no carrinho, o usuário visualizará a quantidade 		a ser somada no empréstimo, caso o valor do dinheiro esteja vazio.
## 
- **Firebase:**
	- Auth:
		- Nessa aplicação deixei apenas disponível o login com email e senha.
		- Opção de recuperar senha do email ativa.
	- Storage:
		- Todas as imagens da aplicação relativa aos produtos e categorias, estão salvas no Storage do Firebase e são retornadas para aplicação a 			partir do link  presente nos 	documents referenciando o link delas.
	- Firestore
		- O coração da aplicação onde todas as informações estão sendo relacionadas.
		- As coleções principais são:
			- Users
			- Products
			- Coupons
			- Orders
	- Users:
		- A coleção de usuário se estende da seguinte forma:
		- Ao criar um usuário, um document será gerado com 4 informações:
		- (email, lending, money e name). Essas são as informações primárias de um usuário gerado.
		- O usuário poderá ter   coleções geradas dentro do seu document, são elas:
			- orders: 
				- O orders gerado no usuário criará um document com uma "orderId" que funciona como uma chave estrangeira, a "orderId" retorna o 					documentID de um Document presente na coleção "Orders" relativo as compras de cada usuário.
			- cart:
				- A coleção "cart" do usuário gerará um document a cada produto adicionado no carrinho de compras, reativo a mudanças feitas na 					aplicação.
				- os seguintes dados de cada document são:
					- category (categoria do produto)
					- pid ( id do produto)
					- product[map] ( description, price, title)
					- quantity (quantidade do produto)
					- size (opção selecionada do produto)

	- Products:
		- A coleção produtos armazena cada um dos produtos das aplicações subdivididos por categorias, os documents adicionados são relativos às 			categorias primárias com duas 	informações:
			- Icon (imagem da categoria)
			- Title (título da categoria)

		- Dentro do document da categoria, se a mesma tiver algum produto, uma outra coleção com o nome "items" será gerada com as seguintes 				informações:
			- (Coleção) Items:
				- (Documents) Documentos de cada produto:
				- Cada documento é relativo a um produto, dentro de uma categoria, e seus dados disponíveis para visualização, são:
				- description (Descrição do produto, exemplo: "Detroit: Become Human, um game com foco na narrativa e escolhas morais")
				- images[] (array de imagens para visualização do produto a partir de um carousel)			- price (preço do produto)								- sizes[] (opções disponíveis)
				- Title (Título da aplicação)
	
		

	- Coupons: 
		- A coleção "Coupons" é utilizada apenas no momento da compra e possui a seguinte estrutura:
			- Coleção (Coupons)
				- Document (documentos relativo a cada desconto)
					- percent (informação que revela um inteiro, entendido no 	app como um percentual de desconto em um produto, pode ser observado a 						na classe "DiscountCard" e no "CartModel", o método 	getDiscount() atualiza um "return getproductsPrice() * discountPercentage 						/ 100;", retornando o valor atualizado em tempo real na aplicação, caso o desconto digitado seja válido no Firestore.

	- Orders
		- A coleção Orders é responsável por registrar na aplicação todas as compras feitas no app de todos os usuários, pensado diretamente para uma 			visualização administrativa, retornando vários fatores importantes como compras totais individuais, criando a 	possibilidade da geração de 			estatísticas baseada no uso de cada cliente.
		- Dentro da coleção Orders, temos as seguintes informações:
			- Coleção (Orders):
				- Document (cada ordem de pedido)
					- clientId ( identificador do cliente comprador)
					- discount (percentual de desconto da compra)
					- category (documentID da categoria)
					- pid (documentID do produto)
					- products[map] (informações de cada produto comprado, descritas na aba Products):
					- productsPrice (valor parcial do produto sem custo de entrega)
					- shipPrice (valor da entrega)
					- status (informação de entrega possuindo 3 opções):
						- 1: prepação
						- 2: transporte
						- 3: entrega
					- totalPrice: (valor da compra) 
## 
São essas as informações principais da aplicação, qualquer dúvida estarei disponível!
## 
Att: Wallace Nery, José Roberto e Pedro Henrique
