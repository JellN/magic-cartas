import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/models/cart_model.dart';
import 'package:loja_online/models/product_model.dart';
import 'package:loja_online/tiles/category_tile.dart';
import 'package:loja_online/widgets/category_widget.dart';
import 'package:scoped_model/scoped_model.dart';

class ProductsGTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ProductModel>(
        builder: (context, child, model) {
      return FutureBuilder<QuerySnapshot>(
        future: model.getCategories(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                /* Container(
                        margin: EdgeInsets.all(5.0),
                        padding: EdgeInsets.all(5.0),
                        height: MediaQuery.of(context).size.height * 0.08,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(20.0)),
                            border: Border.all(
                                color: Colors.grey[500],
                                width: 2.0)),
                        child: Center(
                            child:  TextField(

                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  hintText: "Pesquisar",
                                  hintStyle: TextStyle(color: Colors.black),
                                  icon: Icon(Icons.search, color: Colors.black,),
                                  border: InputBorder.none
                              ),
                            )
                        ),


                      ),*/

                Container(
                  padding: EdgeInsets.only(top: 5.0),
                  height: MediaQuery.of(context).size.height * 0.15,

                  // physics: NeverScrollableScrollPhysics(),

                  child: GridView.builder(
                      padding: EdgeInsets.all(2.0),
                      scrollDirection: Axis.horizontal,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 1.0,
                        crossAxisSpacing: 1.0,
                        childAspectRatio:
                            (MediaQuery.of(context).size.width * 2) /
                                (MediaQuery.of(context).size.height),
                      ),
                      itemCount: snapshot.data!.docs.length,
                      itemBuilder: (context, index) {
                        return CategoryTile(snapshot.data!.docs[index]);
                      }),
                ),
                Divider(
                  height: 5.0,
                  color: Colors.grey,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    margin: EdgeInsets.all(5.0),
                    child: CategoryWidget(snapshot.data!.docs[3]),
                  ),
                )
              ],
            );

            /*   var dividedTiles = ListTile.divideTiles(
                  tiles: snapshot.data.documents.map((doc) {
                    return CategoryTile(doc);
                  }).toList(),
                  color: Colors.grey[500])
              .toList();
          return ListView(
            children: dividedTiles,
          );*/
          }
        },
      );
    });
  }
}
