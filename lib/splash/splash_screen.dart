import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/screens/home_screen.dart';

class Splash extends StatefulWidget {
  Splash({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with TickerProviderStateMixin {

  @override
  @override
  Widget build(BuildContext context) {
    return _introScreen();
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 2)).then((_) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
    });
  }
}

Widget _introScreen() {
  return Stack(
    children: <Widget>[
      Container(
          child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              width: 300,
              height: 250,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/logo.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
        ],
      )),
    ],
  );
}
