import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/models/cart_model.dart';
import 'package:loja_online/models/user_model.dart';
import 'package:loja_online/screens/cart_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:uuid/uuid.dart';

class CartButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CartModel>(
      builder: (context, child, model) {
        int p = model.products.length;
        return Stack(
          children: [
            Positioned(
              left: 7.0,
              child: Text(
                "${model.products.length}",
                style: TextStyle(color: Colors.red, fontSize: 12.0),
              ),
            ),
            FloatingActionButton(
              child: Stack(
                children: [
                  Icon(
                    Icons.add_shopping_cart,
                    color: Colors.white,
                  ),
                ],
              ),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => CartScreen()));
              },
              backgroundColor: Colors.transparent,
            )
          ],
        );
      },
    );
  }
}
