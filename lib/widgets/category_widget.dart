import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/datas/products_data.dart';
import 'package:loja_online/models/cart_model.dart';
import 'package:loja_online/models/product_model.dart';
import 'package:loja_online/tiles/products_tile.dart';

class CategoryWidget extends StatelessWidget {
  final DocumentSnapshot snapshot;

  CategoryWidget(this.snapshot);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: ProductModel.of(context).comentID != null
          ? ProductModel.of(context).getProducts()
          : FirebaseFirestore.instance
              .collection("products")
              .doc(snapshot.id)
              .collection("items")
              .get(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.black,
                ),
            padding: EdgeInsets.all(4.0),
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              ProductData data =
                  ProductData.fromDocument(snapshot.data!.docs[index]);
              data.category = this.snapshot.id;

              return ProductsTile("list", data);
            });
      },
    );
  }
}
