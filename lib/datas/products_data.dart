import 'package:cloud_firestore/cloud_firestore.dart';

class ProductData {
  String? id;
  String? title;
  String? description;
  double? price;
  String? cod;
  List? images;
  List? sizes;
  String? category;

  ProductData.fromDocument(DocumentSnapshot snapshot) {
    id = snapshot.id;
    title = snapshot["title"];
    description = snapshot["description"];
    price = snapshot["price"] + 0.0;
    images = snapshot["images"];
    sizes = snapshot["sizes"];
  }

  Map<String, dynamic> toResumedMap() {
    return {
      "title": title,
      "description": description,
      "price": price,
      "cod": cod,
    };
  }
}
