import 'package:cloud_firestore/cloud_firestore.dart';

class UserData {
  String? id;
  String? name;
  double? money;
  double? lending;

  UserData.fromDocument(DocumentSnapshot snapshot) {
    id = snapshot.id;
    name = snapshot["name"];
    money = snapshot["money"] + 0.0;
    lending = snapshot["lending"] + 0.0;
  }
}
