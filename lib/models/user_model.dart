import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';

class UserModel extends Model {
  bool isLoading = false;

  FirebaseAuth _auth = FirebaseAuth.instance;
  Map<String, dynamic> userData = Map();
  User? firebaseUser = FirebaseAuth.instance.currentUser;

  static UserModel of(BuildContext context) =>
      ScopedModel.of<UserModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
    _loadCurrentUser();
  }

//cadastro no firebase
  void SignUp(
      {required Map<String, dynamic> userData,
      required String pass,
      required VoidCallback onSuccess,
      required VoidCallback onFail}) {
    isLoading = true;
    notifyListeners();
    _auth
        .createUserWithEmailAndPassword(
            email: userData["email"], password: pass)
        .then((user) async {
      await _saveUserData(userData);
      onSuccess();
      isLoading = false;
      notifyListeners();
    }).catchError((e) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

//login
  void SignIn(
      {required email,
      required String pass,
      required VoidCallback onSuccess,
      required VoidCallback onFail}) async {
    isLoading = true;
    notifyListeners();
    _auth
        .signInWithEmailAndPassword(email: email, password: pass)
        .then((user) async {
      await _loadCurrentUser();
      onSuccess();
      isLoading = false;
      notifyListeners();
    }).catchError((e) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

//recuperar senha
  void recoverPass(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

//verificar login
  bool isLoggedIn() {
    return FirebaseAuth.instance.currentUser != null;
  }

//salvar dados de usuário
  Future<Null> _saveUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    await FirebaseFirestore.instance
        .collection("users")
        .doc(_auth.currentUser?.uid)
        .set(userData);
  }

//carregar user atual
  Future<Null> _loadCurrentUser() async {
    if(firebaseUser==null) firebaseUser = _auth.currentUser;
    if (_auth.currentUser  != null) {
      if (userData["name"] == null) {
        print("estou aqui");
        DocumentSnapshot docUser = await FirebaseFirestore.instance
            .collection("users")
            .doc(_auth.currentUser?.uid)
            .get();
        userData = docUser.data() as Map<String, dynamic>;
      }
    }
    notifyListeners();
  }

//sair do usuário
  void signOut() async {
    await _auth.signOut();
    userData = Map();
    notifyListeners();
  }
}
