import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/datas/cart_product.dart';
import 'package:loja_online/datas/user_data.dart';
import 'package:loja_online/models/user_model.dart';
import 'package:scoped_model/scoped_model.dart';

class CartModel extends Model {
  UserModel user;
  bool isLoading = false;
  UserData? userdata;

  List<CartProduct> products = [];
  String? comentID;
  double? lending = 0;

  String? couponCode;
  int discountPercentage = 0;

//verificação de login baseado no UserModel
  CartModel(this.user) {
    if (user.isLoggedIn()) {
      _loadCartItems();
      loadUserWallet();
    }
  }

  static CartModel of(BuildContext context) =>
      ScopedModel.of<CartModel>(context);

//adicionar produto no carrinho
  void addCartItem(CartProduct cartProduct) {
    products.add(cartProduct);
    FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("cart")
        .add(cartProduct.toMap())
        .then((doc) {
      cartProduct.cid = doc.id;
    });
    notifyListeners();
  }

//remover item do carrinho
  void removeCartItem(CartProduct cartProduct) {
    FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("cart")
        .doc(cartProduct.cid)
        .delete();
    products.remove(cartProduct);
    notifyListeners();
  }

//diminuir quantidade do produto no carrinho
  void decProduct(CartProduct cartProduct) {
   cartProduct.quantity = (cartProduct.quantity!-1);
    FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("cart")
        .doc(cartProduct.cid)
        .update(cartProduct.toMap());
    notifyListeners();
  }

//aumentar quantidade de produto no carrinho
  void incProduct(CartProduct cartProduct) {
    cartProduct.quantity = (cartProduct.quantity!+1);
    FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("cart")
        .doc(cartProduct.cid)
        .update(cartProduct.toMap());
    notifyListeners();
  }

//selecionar um cupom de desconto
  void setCoupon(String? couponCode, int discountPercentage) {
    this.couponCode = couponCode;
    this.discountPercentage = discountPercentage;
  }

//atualizar valores
  void updatePrices() {
    notifyListeners();
  }

//retornar variáveis de identificação de categorias para retorno de produtos
  void setProduct(DocumentSnapshot snapshot) {
    this.comentID = snapshot.id;
    getProducts();

    notifyListeners();
  }

//retornar preços do produto baseado na quantidade
  double getproductsPrice() {
    double price = 0.0;
    for (CartProduct c in products) {
      if (c.productData != null) price += (c.quantity! * c.productData!.price!);
    }
    return price;
  }

//retornar disconto baseado no cupom selecionado
  double getDiscount() {
    return getproductsPrice() * discountPercentage / 100;
  }

//valor padrão de entrega
  double getShipPrice() {
    return 9.99;
  }

//atualizar retorno da carteira
  double getWallet() {
    return userdata!.money! -
        (getproductsPrice() + getShipPrice() - getDiscount());
  }

//atualizar retorno do emprestimo baseado no valor disponível na carteira
  double? getLending() {
    if (getWallet() < 0) {
      lending = (getWallet() * -1);
    } else {
      lending = 0;
    }
    return lending;
  }

//finalizar pedido
  Future<String?> finishOrder() async {
    FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .update({
      "money": getWallet() < 0 ? 0 : getWallet(),
      "lending": getLending()! + userdata!.lending!
    });
    if (products.length == 0) return null;
    isLoading = true;
    notifyListeners();
    double productsPrice = getproductsPrice();
    double shiprice = getShipPrice();
    double discount = getDiscount();
    DocumentReference refOrder =
        await FirebaseFirestore.instance.collection("orders").add({
      "clientId": user.firebaseUser?.uid,
      "products": products.map((cartProduct) => cartProduct.toMap()).toList(),
      "shipPrice": shiprice,
      "productsPrice": productsPrice,
      "discount": discount,
      "totalPrice": productsPrice - discount + shiprice,
      "status": 1
    });
    await FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("orders")
        .doc(refOrder.id)
        .set({"orderId": refOrder.id});

    QuerySnapshot query = await FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("cart")
        .get();

    for (DocumentSnapshot doc in query.docs) {
      doc.reference.delete();
    }

    products.clear();
    couponCode = null;

    discountPercentage = 0;
    isLoading = false;

    _loadCartItems();
    notifyListeners();

    return refOrder.id;
  }

//carregar itens do carrinho, função inicada no InitState
  void _loadCartItems() async {
    QuerySnapshot query = await FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .collection("cart")
        .get();

    products =
        query.docs.map((doc) => CartProduct.fromDocument(doc)).toList();
    notifyListeners();
  }

//atualização da carteira
  void loadUserWallet() async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection("users")
        .doc(user.firebaseUser?.uid)
        .get();
    userdata = UserData.fromDocument(documentSnapshot);
  }

//retornar listas de categorias
  Future<QuerySnapshot> getCategories() async {
    return await FirebaseFirestore.instance.collection("products").get();
  }

//retornar listas de produtos baseado na categoria selecionada
  Future<QuerySnapshot> getProducts() async {
    return await FirebaseFirestore.instance
        .collection("products")
        .doc(comentID)
        .collection("items")
        .get();
  }
}
