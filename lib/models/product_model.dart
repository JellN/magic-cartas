import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/datas/cart_product.dart';
import 'package:loja_online/datas/products_data.dart';
import 'package:loja_online/models/user_model.dart';
import 'package:scoped_model/scoped_model.dart';

class ProductModel extends Model {
  UserModel? user;
  bool isLoading = false;

  List<ProductData?> products = [];
  String? comentID;
  String? select;

  ProductModel(this.user) {}

  static ProductModel of(BuildContext context) =>
      ScopedModel.of<ProductModel>(context);

  //selecionar ID da categorias de produtos
  void setProduct(DocumentSnapshot snapshot) {
    this.comentID = snapshot.id;
    getProducts();
    notifyListeners();
  }

//retornar categorias
  Future<QuerySnapshot> getCategories() async {
    return await FirebaseFirestore.instance.collection("products").get();
  }

//retornar produtos baseado na categoria
  Future<QuerySnapshot> getProducts() async {
    return await FirebaseFirestore.instance
        .collection("products")
        .doc(comentID)
        .collection("items")
        .get();
  }
}
