import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PersonTile extends StatelessWidget {
  final DocumentSnapshot snapshot;

  PersonTile(this.snapshot);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(
              Icons.account_balance_wallet,
              size: 80.0,
              color: Theme.of(context).primaryColor,
            ),
            SizedBox(height: 20.0),
            Text(
              snapshot["name"],
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0),
            ),
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text("Dinheiro"),
                Text("R\$ ${snapshot["money"].toStringAsFixed(2)}")
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text("Empréstimo"),
                Text("R\$ ${snapshot["lending"].toStringAsFixed(2)}")
              ],
            ),
          ]),
    );

    Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          /*  SizedBox(
            height: 100.0,
            child: Image.network(snapshot.data["image"], fit: BoxFit.cover),
          ),*/
          Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  snapshot["name"],
                  textAlign: TextAlign.start,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
                ),
                Text(
                  snapshot["email"],
                  textAlign: TextAlign.start,
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(snapshot["money"].toString()),
              Text(snapshot["lending"].toString()),
            ],
          )
        ],
      ),
    );
  }
}
