import 'package:flutter/material.dart';
import 'package:loja_online/datas/products_data.dart';
import 'package:loja_online/screens/product_screen.dart';

class ProductsTile extends StatelessWidget {
  final String type;
  final ProductData product;

  ProductsTile(this.type, this.product);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ProductGScreen(product)));
      },
      child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Image.network(
                  product.images?[0],
                  fit: BoxFit.fill,
                  height: 210.0,
                  width: 200.0,
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        product.title!,
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                      Text(
                        "R\$ ${product.price?.toStringAsFixed(2)}",
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Divider(),
                      Text(
                        product.description!,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.black),
                      ),
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }
}
