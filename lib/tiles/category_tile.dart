import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/models/cart_model.dart';
import 'package:loja_online/models/product_model.dart';
import 'package:loja_online/screens/category_screen.dart';

class CategoryTile extends StatefulWidget {
  final DocumentSnapshot snapshot;

  CategoryTile(this.snapshot);

  @override
  _CategoryTileState createState() => _CategoryTileState(snapshot);
}

class _CategoryTileState extends State<CategoryTile> {
  final DocumentSnapshot snapshot;

  _CategoryTileState(this.snapshot);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          setState(() {
            ProductModel.of(context).setProduct(snapshot);
          });
          //select = snapshot.documentID;
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              radius: 29.0,
              backgroundColor:
                  ProductModel.of(context).comentID == snapshot.id
                      ? Theme.of(context).primaryColor
                      : Colors.transparent,
              child: CircleAvatar(
                  radius: 27.0,
                  backgroundColor: Colors.transparent,
                  backgroundImage: NetworkImage(snapshot["icon"])),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(2.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      snapshot["title"],
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
    /*ListTile(
      leading: CircleAvatar(
        radius: 25.0,
        backgroundColor: Colors.transparent,
        backgroundImage: NetworkImage(snapshot.data["icon"]),
      ),
      title: Text(snapshot.data["title"]),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: (){
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context)=>CategoryScreen(snapshot))
        );

      },




    );*/
  }
}
