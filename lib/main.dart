import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:loja_online/datas/products_data.dart';
import 'package:loja_online/models/product_model.dart';
import 'package:loja_online/screens/home_screen.dart';
import 'package:loja_online/screens/login_screen.dart';
import 'package:loja_online/screens/signup_screen.dart';
import 'package:loja_online/splash/splash_screen.dart';
import 'package:scoped_model/scoped_model.dart';

import 'models/cart_model.dart';
import 'models/user_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: ScopedModelDescendant<UserModel>(builder: (context, child, model) {
        return ScopedModel<CartModel>(
            model: CartModel(model),
            child: ScopedModel<ProductModel>(
              model: ProductModel(model),
              child: MaterialApp(
                title: "Magic Store",
                theme: ThemeData(
                    primarySwatch: Colors.green, primaryColor: Colors.blue),
                debugShowCheckedModeBanner: false,
                home: Splash(title: "Inicio",),
              ),
            ));
      }),
    );
  }
}
