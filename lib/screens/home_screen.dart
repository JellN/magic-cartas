import 'package:flutter/material.dart';
import 'package:loja_online/models/cart_model.dart';
import 'package:loja_online/models/user_model.dart';
import 'package:loja_online/tabs/home_tab.dart';
import 'package:loja_online/tabs/orders_tab.dart';
import 'package:loja_online/tabs/wallet_tab.dart';
import 'package:loja_online/tabs/products_tab.dart';
import 'package:loja_online/widgets/cart_button.dart';
import 'package:loja_online/widgets/custom_drawer.dart';
import 'package:scoped_model/scoped_model.dart';

import 'cart_screen.dart';

class HomeScreen extends StatelessWidget {
  final _pageController = PageController();
  int p = 0;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CartModel>(builder: (context, child, model) {
      p = model.products.length;

      return Scaffold(
        body: PageView(
          controller: _pageController,
          children: [
            Scaffold(
              appBar: AppBar(
                title: Text("Magic Store"),
                centerTitle: true,
              ),
              drawer: CustomDrawer(_pageController),
              body: ProductsGTab(),
            ),
            Scaffold(
              appBar: AppBar(
                title: Text("Compras"),
                centerTitle: true,
              ),
              drawer: CustomDrawer(_pageController),
              body: OrdersTab(),
            ),
            CartScreen(),
            Scaffold(
              appBar: AppBar(
                title: Text("Carteira"),
                centerTitle: true,
              ),
              drawer: CustomDrawer(_pageController),
              body: WalletTab(),
            ),
          ],
        ),
        bottomNavigationBar: AnimatedBuilder(
            animation: _pageController,
            builder: (context, snapshot) {
              return BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  currentIndex: _pageController?.page?.round() ?? 0,
                  onTap: (index) {
                    _pageController.animateToPage(index,
                        curve: Curves.easeInOutQuint,
                        duration: Duration(milliseconds: 500));
                  },
                  items: [
                    BottomNavigationBarItem(
                      icon: Icon(Icons.home),
                      label: "Home"
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.shop),
                      label: "Compras",
                    ),
                    BottomNavigationBarItem(
                      icon: Stack(
                        children: <Widget>[
                          Icon(Icons.shopping_cart),
                          p > 0
                              ? Positioned(
                                  right: 0,
                                  child: Container(
                                    padding: EdgeInsets.all(1),
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    constraints: BoxConstraints(
                                      minWidth: 12,
                                      minHeight: 12,
                                    ),
                                    child: Text(
                                      '${model.products.length}',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 8,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                )
                              : Positioned(
                                  right: 0,
                                  child: Container(),
                                )
                        ],
                      ),
                      label: 'Carrinho',
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.attach_money),
                      label: "Carteira",
                    ),
                  ]);
            }),
      );
    });

    /*PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
    Scaffold(

      */ /*floatingActionButton: FloatingActionButton(
        onPressed: (){
          _pageController.jumpToPage(1);
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
      ),*/ /*


    ),
        Scaffold(
          appBar: GradientAppBar(
            gradient: LinearGradient(colors: [Colors.green, Colors.blue]),
            title: Text("Produtos"),
            centerTitle: true,

          ),

          drawer: CustomDrawer(_pageController),
          body: ProductsGTab(),

        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Lojas"),
            centerTitle: true,

          ),


          body: PlacesTab(),
          drawer: CustomDrawer(_pageController),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Meus pedidos"),
            centerTitle: true,
          ),
          body: OrdersTab(),
          drawer: CustomDrawer(_pageController),
        )



      ],


    );*/
  }
}
